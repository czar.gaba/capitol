<?php require ("connection.php"); ?>
<?php
session_start();
if(isset($_SESSION['email'])){
}else{
	header('location: index.php');
}
?>
<?php
	date_default_timezone_set('asia/manila');
	include 'comments.ink.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ADMIN - Capitol</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="admin-page.php">Admin Panel</a>
				<a class="navbar-brand" href="updates.php">Updates</a>
				<a class="navbar-brand" href="index.php">Home</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
               
               
                <li class="dropdown">
                 
                        <li>
                            <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
				
				
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li class="active">
                        <a href="admin-page.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#posts_dropdown"><i class="fa fa-fw fa-arrows-v"></i> Posts <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="posts_dropdown" class="collapse">
                            <li>
                                <a href="posts.php">View Posts</a>
                            </li>
                            <li>
                             
                            </li>
                        </ul>
                    </li>
                    <li>
                        <a href="categories.php"><i class="fa fa-fw fa-wrench"></i> Categories</a>
                    </li>
                    
                    <li class="">
<!--                        <a href="comments.php"><i class="fa fa-fw fa-file"></i> Comments</a>-->
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Projects<i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="projects-approved.php">Approved Project</a>
                            </li>
                            <li>
                              <a href="projects-declined.php">Declined Project</a>
                            </li>
                        </ul>
                    </li>
 <?php include('municipality-menu.php');?>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                           Aproved Projects
                            <small>Admin</small>
                        </h1>



						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>Id</th>
									<th>Author</th>
									<th>Title</th>
									<th>Image</th>
									<th>Comments</th>
									<th>Date</th>
									<th>Municipalities</th>
									<th>Start Date</th>	
									<th>End Date</th>
<!--									<th>Status</th>-->
								</tr>
							</thead>
							
							
							<tbody>
							
<?php  
	$query = "SELECT * FROM posts WHERE status = 'Approve'";
	$select_posts = mysqli_query($dbcon, $query);
	
	 while ($row = mysqli_fetch_assoc($select_posts)) {
		$post_id = $row['post_id'];
		$post_author = $row['post_author'];
		$post_title = $row['post_title'];
		$post_image = $row['post_image'];
		$post_comment_count = $row['post_comment_count'];
		$post_date = $row['post_date'];
		$post_municipalities = $row['post_municipalities'];
		$start_date = $row['start-date'];
		$end_date = $row['end-date'];
		
		echo "<tr>";
		echo "<td>$post_id</td>";
		echo "<td>$post_author</td>";
		echo "<td>$post_title</td>";
		echo "<td><img width='80' src='images/$post_image' alt='image'></td>";
		echo "<td>$post_comment_count</td>";
		echo "<td>$post_date</td>";
		echo "<td>$post_municipalities</td>";
		echo "<td>$start_date</td>";
		echo "<td>$end_date</td>";
//		echo "<td><a class='btn btn-primary' href='approve.php?approve=$post_id'>Approve</a></td>";
//		echo "<td><a class='btn btn-danger' href='disapprove.php?disapprove=$post_id'>Disapprove</a></td>";
		echo "</tr>";
		
	}


?>							


					
							</tbody>
							</table>
							


                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
