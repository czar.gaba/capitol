<?php
session_start();
ob_start();
include('connection.php');
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>CAGAYAN - Project impact monitoring system</title>
    <meta name="description" content="Free Bootstrap Theme by BootstrapMade.com">
    <meta name="keywords" content="free website templates, free bootstrap themes, free template, free bootstrap, free website template">
    
    <link rel="stylesheet" type="text/css" href="https://fonts.googleapis.com/css?family=Open+Sans|Candal|Alegreya+Sans">
    <link rel="stylesheet" type="text/css" href="css/font-awesome.min.css">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/imagehover.min.css">
    <link rel="stylesheet" type="text/css" href="css/style.css">

  </head>
  <body>
    <!--Navigation bar-->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="index.php">CAGAYAN<span>VALLEY</span></a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="./">Home</a></li>
          <li><a href="#courses">Municipalities</a></li>
          <li><a href="#pricing">About Us</a></li>
          <li><a href="#" data-target="#login" data-toggle="modal">Sign in</a></li>
<!--          <li class="btn-trial"><a href="#footer">Contact Us</a></li>-->
        </ul>
        </div>
      </div>
    </nav>
    <!--/ Navigation bar-->
    <!--Modal box-->
    <div class="modal fade" id="login" role="dialog">
      <div class="modal-dialog modal-sm">
      
        <!-- Modal content no 1-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-center form-title">Login</h4>
          </div>
          <div class="modal-body padtrbl">

            <div class="login-box-body">
              <p class="login-box-msg">Sign in to start your session</p>
              <div class="form-group">
                <form role="form" action="index.php" method="post">
                 <div class="form-group has-feedback"> <!----- username -------------->
                      <input class="form-control" placeholder="Email Address" name="email" type="email /> 
            <span style="display:none;font-weight:bold; position:absolute;color: red;position: absolute;padding:4px;font-size: 11px;background-color:rgba(128, 128, 128, 0.26);z-index: 17;  right: 27px; top: 5px;" id="span_loginid"></span><!---Alredy exists  ! -->
                      <span class="glyphicon glyphicon-user form-control-feedback"></span>
                  </div>
                  <div class="form-group has-feedback"><!----- password -------------->
                      <input class="form-control" placeholder="Password" name="password" type="password" />
            <span style="display:none;font-weight:bold; position:absolute;color: grey;position: absolute;padding:4px;font-size: 11px;background-color:rgba(128, 128, 128, 0.26);z-index: 17;  right: 27px; top: 5px;" id="span_loginpsw"></span><!---Alredy exists  ! -->
                      <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                  </div>
                  <div class="row">
                      <div class="col-xs-12">
                          <div class="checkbox icheck">
                          </div>
                      </div>
                      <div class="col-xs-12">
                          <div class="form-group"><input class="btn btn-success btn-block" name="login" value="login" type="Submit"></div>
                      </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
    <!--/ Modal box-->
	
	 <!--Modal box for muicipality-->
    <div class="modal fade" id="login1" role="dialog">
      <div class="modal-dialog modal-sm">
      
        <!-- Modal content no 1-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title text-center form-title">Login</h4>
          </div>
          <div class="modal-body padtrbl">

            <div class="login-box-body">
              <p class="login-box-msg">Sign in to start your session</p>
              <div class="form-group">
                <form role="form" action="index.php" method="post">
                 <div class="form-group has-feedback"> <!----- username -------------->
                      <input class="form-control" placeholder="Email Address" name="email" type="email /> 
            <span style="display:none;font-weight:bold; position:absolute;color: red;position: absolute;padding:4px;font-size: 11px;background-color:rgba(128, 128, 128, 0.26);z-index: 17;  right: 27px; top: 5px;" id="span_loginid"></span><!---Alredy exists  ! -->
                      <span class="glyphicon glyphicon-user form-control-feedback"></span>
                  </div>
                  <div class="form-group has-feedback"><!-- password -->
                      <input class="form-control" placeholder="Password" name="password" type="password" />
            <span style="display:none;font-weight:bold; position:absolute;color: grey;position: absolute;padding:4px;font-size: 11px;background-color:rgba(128, 128, 128, 0.26);z-index: 17;  right: 27px; top: 5px;" id="span_loginpsw"></span><!---Alredy exists  ! -->
                      <span class="glyphicon glyphicon-lock form-control-feedback"></span>
                  </div>
                  <div class="row">
                      <div class="col-xs-12">
                          <div class="checkbox icheck">
                          </div>
                      </div>
                      <div class="col-xs-12">
                          <div class="form-group"><input class="btn btn-success btn-block" name="login1" value="login" type="Submit"></div>
                      </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
    <!--/ Modal box-->
	
    <!--Banner-->
    <div class="banner">
      <div class="bg-color">
        <div class="container">
          <div class="row">
            <div class="banner-text text-center">
              <div class="text-border">
                <h2 class="te xt-dec">Cagayan Valley</h2>
              </div>
              <div class="intro-para text-center quote">
                <p class="big-text">Project Impact Monitoring System.</p>
<!--                <a href="#footer" class="btn get-quote">Contact Us</a>-->
              </div>
              <a href="#feature" class="mouse-hover" style="display:none;"><div class="mouse"></div></a>
            </div>
          </div>
        </div>
      </div>
    </di v>
    <!--/ Banner-->
    
    <!--Courses-->
    <section id ="courses" class="section-padding">
      <div class="container">
        <div class="row">
          <div class="header-section text-center">
            <h2>Municipalities</h2>
            <p>Municipalities of Cagayan Valley, Philippines<br> Sulong...Cagayan...</p>
            <hr class="bottom-line">
          </div>
        </div>
      </div>
      <div class="container">
        
         
          
		  <?php 	$query = "SELECT * FROM tblmunicipalities ";
  $select_posts = mysqli_query($dbcon, $query);
  while ($row = mysqli_fetch_assoc($select_posts)) {
    $img = $row['img'];
    if($img != ""){
    ?>
		  <div class="col-md-4 col-sm-6 padleft-right">
            <figure class="imghvr-fold-up">
              <img src="municipality/<?php echo $row['img']; ?>" class="img-responsive">
              <figcaption>
                  <h3> <?php echo $row['municipalities'];?></h3>
                  <p> <?php echo $row['municipalities'];?> is _______.</p>
              </figcaption> 
             <a href="#" data-target="#login1" data-toggle="modal"></a>
            </figure>
      </div>
  <?php }else{
 ?>
 <div class="col-md-4 col-sm-6 padleft-right">
       <figure class="imghvr-fold-up">
         <img src="municipality/xx.jpg" class="img-responsive">
         <figcaption>
             <h3> <?php echo $row['municipalities'];?></h3>
             <p> Alcala is _______.</p>
         </figcaption> 
        <a href="#" data-target="#login1" data-toggle="modal"></a>
       </figure>
 </div>
<?php
  }
  //end loop
  } ?>
          
        </div>
      </div>
    </section>
    <!--/ Courses-->
    <!--Pricing-->
    <section id ="pricing" class="section-padding">
      <div class="container">
        <div class="row">
          <div class="header-section text-center">
            <h2>About Us</h2>
            <p>Project impact monitoring system is,<br> description here!.</p>
            <hr class="bottom-line">
          </div>
          <div class="col-md-4 col-sm-4">
            <div class="price-table">
              <!-- Plan  -->
              <div class="pricing-head">
                <h4>Vision - Mission</h4>
                <span>Our Vision and Mission Description Here</span> 
              </div>
          
              <!-- Plean Detail -->
              <div class="price-in mart-15">
                <a href="vision-mission.php" class="btn btn-bg green btn-block">Real More</a> 
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-4">
            <div class="price-table">
              <!-- Plan  -->
              <div class="pricing-head">
                <h4>Municipalities</h4>
                <span>See All The List of Municipalities in Cagayan.</span> 
              </div>
          
              <!-- Plean Detail -->
              <div class="price-in mart-15">
                <a href="#courses" class="btn btn-bg yellow btn-block">See All</a> 
              </div>
            </div>
          </div>
          <div class="col-md-4 col-sm-4">
            <div class="price-table">
              <!-- Plan  -->
              <div class="pricing-head">
                <h4>Map Location</h4>
                
                <span>See Us on map. Explore Cagayan Valley</span> 
              </div>
          
              <!-- Plean Detail -->
              <div class="price-in mart-15">
                <a href="https://www.google.com/maps/place/Cagayan/@18.7346893,120.5277034,8z/data=!3m1!4b1!4m5!3m4!1s0x3385f93fc78b3a2d:0x8544d56b729cc060!8m2!3d18.2489629!4d121.8787833?dcr=0" class="btn btn-bg red btn-block">Explore</a> 
              </div>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!--/ Pricing-->
    <!--Contact-->
    <section id ="contact" class="section-padding">
      <div class="container">
        
      </div>
    </section>
    <!--/ Contact-->
    <!--Footer-->
    <footer id="footer" class="footer">
      <div class="container text-center">
    
      <div class="row">
      
          
          
        </div>

        ©2017 CagayanValley. All rights reserved
        <div class="credits">

            Project Impact Monitoring System | <a href="index.html"> CSU WEBDEV</a>
        </div>
      </div>
    </footer>
    <!--/ Footer-->
    
    <script src="js/jquery.min.js"></script>
    <script src="js/jquery.easing.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/custom.js"></script>
    <script src="contactform/contactform.js"></script>
    
  </body>
</html>

<?php

include ("connection.php");
if (isset($_POST['login']))
{
	$user_email=$_POST['email'];
	$user_password=$_POST['password'];
	$check_user="SELECT * from users WHERE user_email='$user_email' AND user_password='$user_password'";
	$run=mysqli_query($dbcon,$check_user);
	
	if (mysqli_num_rows($run))
	{
		echo "<script>window.open('admin-page.php','_self')</script>";
		$_SESSION['email']=$user_email;
	}
	else{
		echo "<script>alert('Kindly Check Your Login Credentials')</script>";
	}
}
//code for municipality--
if (isset($_POST['login1']))
{
	$user_email=$_POST['email'];
	$user_password=$_POST['password'];
	
	$check_user=" SELECT * from tblmunicipalities WHERE username='$user_email' AND password='$user_password'";
	$run=mysqli_query($dbcon,$check_user);
	
	if (mysqli_num_rows($run))
	{
		echo "<script>window.open('project-form.php','_self')</script>";
		$_SESSION['email']=$user_email;
	}
	else{
		echo "<script>alert('Kindly Check Your Login Credentials')</script>";
	}
}
?>