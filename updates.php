<?php require ("connection.php"); ?>
<?php
session_start();
//if(isset($_SESSION['email'])){
//}else{
//	header('location: index.php');
//}
?>
<?php
	date_default_timezone_set('asia/manila');
	include 'comments.ink.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Capitol - Updates Page</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/blog-post.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.11';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
</head>

<body>

    <!-- Navigation -->
    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="/capitol/admin-page.php">Admin Panel</a>
				<a class="navbar-brand" href="/capitol/updates.php">Updates</a>
				<a class="navbar-brand" href="index.php">Home</a>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>


<!--content    -->
<div class="container">
<div class="col-md-8">
    <ul class="timeline">
       <?php 
    $st="SELECT * FROM `posts` order by post_id desc";
    $select_posts = mysqli_query($dbcon, $st);
	while ($row = mysqli_fetch_assoc($select_posts)) {
        ?>
       
        <li>
          <div class="timeline-badge"><i class="glyphicon glyphicon-check"></i></div>
          <div class="timeline-panel">
            <div class="timeline-heading">
              <h2 class="timeline-title"><?php echo $row['post_title'];?></h2>
              <p><small class="text-muted"><i class="glyphicon glyphicon-time"></i> <?php echo $row['post_date'];?></small></p>
            </div>
            <div class="timeline-body">
                <img src="images/<?php echo $row['post_image'];?>" alt="" style="width:100%;"class="img img-reponsive"/>
              <p><h3><?php echo $row['post_municipalities'];?></h3></p>
              <p><?php echo $row['post_content'];?></p>
            </div>
            <div class="updates">
                <h2>updates</h2>
                <table class="table">
                    <tr>
                        <td>description</td>
                        <td>date</td>
                    </tr>
                    <?php 
                    $id = $row['post_id'];
                    $str = "SELECT * FROM `tbl_update` WHERE `post_id`='$id'";
                    $select_postss = mysqli_query($dbcon, $str);
                    while ($rows = mysqli_fetch_assoc($select_postss)) {
                         ?>
                         <tr>
                             <td><?php echo $rows['description']; ?></td>
                             <td><?php echo $rows['date']; ?></td>

                         </tr>
                         <?php
                     }
                    ?>
                </table>
            </div>
            <div class="comments">
                   <div class="fb-comments" data-href="http://localhost/capitol/viewpost?=<?php echo $row['post_id'];?>" data-numposts="10"></div>
            </div>
          </div>
        </li>
        
        
<?php } ?>
       
    </ul>
</div>

        <!-- Blog Sidebar Widgets Column -->
            <div class="col-md-4">
<?php
	if(isset($_POST['submit'])) {
	$search = $_POST['search'];
	
	$query = "SELECT * FROM posts WHERE post_tags LIKE '%$search%' ";
	$search_query = mysqli_query($dbcon, $query);
	
	$count = mysqli_num_rows($search_query);
	if($count == 0) {
		echo "<h3>NO RESULT</h3>";
	} else {
		echo "<h3>SOME RESULT</h3>";
	}
	
	}
	
	
?>
			
			
			
                <!-- Blog Search Well -->
                <div class="well">
                    <h4>Search Post</h4>
					<form action="search.php" method="post">
                    <div class="input-group">
                        <input name="search" type="text" class="form-control">
                        <span class="input-group-btn">
                            <button name="submit" class="btn btn-default" type="submit">
                                <span class="glyphicon glyphicon-search"></span>
                        </button>
                        </span>
                    </div>

                    <!-- /.input-group -->
                </div>
				</form>

				
				
				
                <!-- Blog Categories Well -->
                <div class="well">
<?php

	$query = "SELECT * FROM category";
	$select_category_sidebar = mysqli_query($dbcon, $query);

?>		

                    <h4>Categories</h4>
                    <div class="row">
                        <div class="col-lg-12">
                            <ul class="list-unstyled">
<?php							
		while($row = mysqli_fetch_assoc($select_category_sidebar)) {
		$cat_title = $row['cat_title'];
	
		echo "<li><a href='#'>$cat_title</a></li>";
		}
?>	

                            </ul>
                        </div>

                </div>
				

<!--
                 Side Widget Well 
			    <div class="well">
                    <h4>Side Widget Well</h4>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Inventore, perspiciatis adipisci accusamus laudantium odit aliquam repellat tempore quos aspernatur vero.</p>
                </div>
-->

            </div>

        </div>
        <!-- /.row -->

</div>
<!--end content-->
     
     
      <footer>
<div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <p>Copyright &copy; 2017|Project impact monitoring system</p>
                </div>
            </div>
            </div>
            <!-- /.row -->
        </footer>

<style>
.timeline {
  list-style: none;
  padding: 20px 0 20px;
  position: relative;
}
.timeline:before {
  top: 0;
  bottom: 0;
  position: absolute;
  content: " ";
  width: 3px;
  background-color: #eeeeee;
  right: 25px;
  margin-left: -1.5px;
}
.timeline > li {
  margin-bottom: 20px;
  position: relative;
}
.timeline > li:before,
.timeline > li:after {
  content: " ";
  display: table;
}
.timeline > li:after {
  clear: both;
}
.timeline > li:before,
.timeline > li:after {
  content: " ";
  display: table;
}
.timeline > li:after {
  clear: both;
}
.timeline > li > .timeline-panel {
  width: calc( 100% - 75px );
  float: left;
  border: 1px solid #d4d4d4;
  border-radius: 2px;
  padding: 20px;
  position: relative;
  -webkit-box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
  box-shadow: 0 1px 6px rgba(0, 0, 0, 0.175);
}
.timeline > li > .timeline-panel:before {
  position: absolute;
  top: 26px;
  right: -15px;
  display: inline-block;
  border-top: 15px solid transparent;
  border-left: 15px solid #ccc;
  border-right: 0 solid #ccc;
  border-bottom: 15px solid transparent;
  content: " ";
}
.timeline > li > .timeline-panel:after {
  position: absolute;
  top: 27px;
  right: -14px;
  display: inline-block;
  border-top: 14px solid transparent;
  border-left: 14px solid #fff;
  border-right: 0 solid #fff;
  border-bottom: 14px solid transparent;
  content: " ";
}
.timeline > li > .timeline-badge {
  color: #fff;
  width: 50px;
  height: 50px;
  line-height: 50px;
  font-size: 1.4em;
  text-align: center;
  position: absolute;
  top: 16px;
  right: 0px;
  margin-left: -25px;
  background-color: #999999;
  z-index: 100;
  border-top-right-radius: 50%;
  border-top-left-radius: 50%;
  border-bottom-right-radius: 50%;
  border-bottom-left-radius: 50%;
}
.timeline > li.timeline-inverted > .timeline-panel {
  float: right;
}
.timeline > li.timeline-inverted > .timeline-panel:before {
  border-left-width: 0;
  border-right-width: 15px;
  left: -15px;
  right: auto;
}
.timeline > li.timeline-inverted > .timeline-panel:after {
  border-left-width: 0;
  border-right-width: 14px;
  left: -14px;
  right: auto;
}
.timeline-badge.primary {
  background-color: #2e6da4 !important;
}
.timeline-badge.success {
  background-color: #3f903f !important;
}
.timeline-badge.warning {
  background-color: #f0ad4e !important;
}
.timeline-badge.danger {
  background-color: #d9534f !important;
}
.timeline-badge.info {
  background-color: #5bc0de !important;
}
.timeline-title {
  margin-top: 0;
  color: inherit;
}
.timeline-body > p,
.timeline-body > ul {
  margin-bottom: 0;
}
.timeline-body > p + p {
  margin-top: 5px;
}
</style>


    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
<script>

</script>

</body>

</html>