<?php require ("connection.php"); ?>
<?php
session_start();
if(isset($_SESSION['email'])){
}else{
	header('location: index.php');
}
?>
<?php
	date_default_timezone_set('asia/manila');
	include 'comments.ink.php';
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>ADMIN - Capitol</title>

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body>

    <div id="wrapper">

        <!-- Navigation -->
        <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="admin-page.php">Admin Panel</a>
				<a class="navbar-brand" href="updates.php">Updates</a>
            </div>
            <!-- Top Menu Items -->
            <ul class="nav navbar-right top-nav">
               
               
                <li class="dropdown">
                 
                        <li>
                            <a href="logout.php"><i class="fa fa-fw fa-power-off"></i> Log Out</a>
                        </li>
                    </ul>
                </li>
				
				
            <!-- Sidebar Menu Items - These collapse to the responsive navigation menu on small screens -->
            <div class="collapse navbar-collapse navbar-ex1-collapse">
                <ul class="nav navbar-nav side-nav">
                    <li>
                        <a href="admin-page.php"><i class="fa fa-fw fa-dashboard"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#posts_dropdown"><i class="fa fa-fw fa-arrows-v"></i> Posts <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="posts_dropdown" class="collapse">
                            <li>
                                <a href="posts.php">View Posts</a>
                            </li>
                            <li>
<!--                                <a href="posts.php?source=add_post">Add Posts</a>-->
                            </li>
                        </ul>
                    </li>
                    <li class="active">
                    </li>
                    
                    <li class="">
<!--                        <a href="comments.php"><i class="fa fa-fw fa-file"></i> Comments</a>-->
                    </li>
                    <li>
                        <a href="javascript:;" data-toggle="collapse" data-target="#demo"><i class="fa fa-fw fa-arrows-v"></i> Projects <i class="fa fa-fw fa-caret-down"></i></a>
                        <ul id="demo" class="collapse">
                            <li>
                                <a href="projects-approved.php">Approved Projects</a>
                            </li>
                            <li>
                                <a href="projects-declined.php">Declined Projects</a>
                            </li>
                        </ul>
                    </li>
                     <?php include('municipality-menu.php');?>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </nav>

        <div id="page-wrapper">

            <div class="container-fluid">

                <!-- Page Heading -->
                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">
                            Welcome to Admin Page
                            <small>Admin</small>
                        </h1>
						
						<div class="col-lg-6">
<?php
	if(isset($_POST['submit'])) {
	$cat_title = $_POST['cat_title'];
	
	if($cat_title == "" OR empty($cat_title)) {
		echo "This Field Should not be Empty";
	} else {
		$query = "INSERT INTO category (cat_title)";
		$query .="VALUE('$cat_title')";
		
		$create_category_query = mysqli_query($dbcon, $query);
	}
		
	}

?>						
						
	
						
						<form action="" method="post">
						
                        <?php if(isset($_GET['edit'])){}else{
    ?>
 <div class="form-group hide-cat">
							<label form="cat-title">Add Category</label>
							<input type="text" class="form-control" name="cat_title">
						</div>

<div class="form-group">
							<input class="btn btn-primary" type="submit" name="submit" value="Add Category">
						</div>

<?php
    
}
                            
                            
                            ?>
                       
						
						
						
						</form>
<?php
	if(isset($_GET['edit'])) {
		$cat_id = $_GET['edit'];
		
		include "update_categories.php";
	}

?>						
						

						
						</div>
						<div class="col-lg-6">
			
						
						
						<table class="table table-bordered table-hover">
							<thead>
								<tr>
									<th>ID</th>
									<th>Category Title</th>
								</tr>
							</thead>
							<tbody>
<?php	

		$query = "SELECT * FROM category";
		$select_categories = mysqli_query($dbcon, $query);
		
		while($row = mysqli_fetch_assoc($select_categories)) {
			$cat_id = $row['cat_id'];
			$cat_title = $row['cat_title'];
	
		echo "<tr>";
		echo "<td>$cat_id</td>";
		echo "<td>$cat_title</td>";
		echo "<td><a href='categories.php?delete=($cat_id)'>Delete</td>";
		echo "<td><a class='edit-cat' href='categories.php?edit=($cat_id)'>Edit</td>";
		echo "</tr>";
		}
?>		
<?php

	if(isset($_GET['delete'])) {
		
	$the_cat_id = $_GET['delete'];
		$query = "DELETE FROM category WHERE cat_id = ($the_cat_id)";
			$delete_query = mysqli_query($dbcon, $query);
			
	}

?>



							
							</tbody>
						</table>
								
						
						
						</div>
						
						
						
						
						
						
                    </div>
                </div>
                <!-- /.row -->

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <style>
        .hide{display:none !important;}
    </style>
    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>
    <script>
      $('.edit-cat').click(function(){
          $('.hide-cat').addClass('hide');
      });
    </script>
</body>

</html>
